require 'rails_helper'
 
RSpec.describe Country, type: :model do
    # pending "add some examples to (or delete) #{__FILE__}"
    describe Country do        
           it { should have_many(:user)}    
           it { should validate_presence_of(:short_code) }
           it { should validate_presence_of(:Phone_code) }
           it { should validate_presence_of(:Full_Name) }
           it { should validate_presence_of(:Currency) }
           it { should validate_presence_of(:language) }
           it { should validate_presence_of(:active) }
           it { should validate_uniqueness_of(:short_code) }
           it { should validate_uniqueness_of(:Phone_code) }
           it { should validate_uniqueness_of(:Full_Name) }
           it { should validate_uniqueness_of(:Currency) }
           it { should validate_inclusion_of(:active).in_array([0, 1])}
           it { should validate_numericality_of(:Phone_code)}
         

    end
  end

  

