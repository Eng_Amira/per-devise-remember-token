require 'rails_helper'

RSpec.describe "user_infos/new", type: :view do
  before(:each) do
    assign(:user_info, UserInfo.new(
      :user_id => 1,
      :mobile => "MyString",
      :address_verification_id => 1,
      :nationalid_verification_id => 1,
      :nationalid_verification_id => 1,
      :selfie_verification => 1,
      :status => 1
    ))
  end

  it "renders new user_info form" do
    render

    assert_select "form[action=?][method=?]", user_infos_path, "post" do

      assert_select "input[name=?]", "user_info[user_id]"

      assert_select "input[name=?]", "user_info[mobile]"

      assert_select "input[name=?]", "user_info[address_verification_id]"

      assert_select "input[name=?]", "user_info[nationalid_verification_id]"

      assert_select "input[name=?]", "user_info[nationalid_verification_id]"

      assert_select "input[name=?]", "user_info[selfie_verification]"

      assert_select "input[name=?]", "user_info[status]"
    end
  end
end
