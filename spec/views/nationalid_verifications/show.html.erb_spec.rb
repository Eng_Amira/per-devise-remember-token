require 'rails_helper'

RSpec.describe "nationalid_verifications/show", type: :view do
  before(:each) do
    @nationalid_verification = assign(:nationalid_verification, NationalidVerification.create!(
      :user_id => 2,
      :legal_name => "Legal Name",
      :national_id => 3,
      :type => 4,
      :status => 5,
      :note => "Note"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Legal Name/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5/)
    expect(rendered).to match(/Note/)
  end
end
