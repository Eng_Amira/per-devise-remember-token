require 'rails_helper'

RSpec.describe "address_verifications/show", type: :view do
  before(:each) do
    @address_verification = assign(:address_verification, AddressVerification.create!(
      :user_id => 2,
      :country_id => 3,
      :city => "City",
      :state => "State",
      :street => "Street",
      :building => "Building",
      :number => "Number",
      :status => 4,
      :note => "Note"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/State/)
    expect(rendered).to match(/Street/)
    expect(rendered).to match(/Building/)
    expect(rendered).to match(/Number/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/Note/)
  end
end
