class Clearance::SessionsController < Clearance::BaseController
    if respond_to?(:before_action)
      before_action :redirect_signed_in_users, only: [:new]
      skip_before_action :require_login,
        only: [:create, :new, :destroy],
        raise: false
      skip_before_action :authorize,
        only: [:create, :new, :destroy],
        raise: false
    else
      before_filter :redirect_signed_in_users, only: [:new]
      skip_before_filter :require_login,
        only: [:create, :new, :destroy],
        raise: false
      skip_before_filter :authorize,
        only: [:create, :new, :destroy],
        raise: false
    end
    skip_before_action :check2fa ,only: [:new, :destroy, :create]
    skip_before_action :check_active_session ,only: [:new, :destroy, :create]
    #before_action :get_two_factor ,:except => [:new , :create	,:destroy	]
    
  # create new session.
  # @param [Integer] email
  # @param [Integer] password 
  # if user credentials are true,he is signed in and redirected to confirmation page
    def create
      @user = authenticate(params)
      @token = SecureRandom.hex(4)

       if @user and @user.status == 0 # prevent user from logging untill confirming his email
        flash.now.notice = 'You should confirm your email first'
        render template: "sessions/new"

       elsif @user and @user.disabled == 1 # prevent user fron logging if his account is locked
          EmailService.new(user_mail:@user.email, subject:'Welcome To Payers', via:'payers@payers.net', token:@user.unlock_token, mailer:'user_mailer', type:'unlockaccount', id:@user.id).call
          flash.now.notice = 'Your account is locked,please visit your email to activate it.'
          render template: "sessions/new"

       else
  
           sign_in(@user) do |status|
            #@device_id = SecureRandom.uuid
                if current_user

                  if @user.loginattempts == 0
                    @create_notification = NotificationsSetting.create(user_id: @user.id.to_i,money_transactions: 1,pending_transactions: 2,transactions_updates: 2,help_tickets_updates: 3,tickets_replies: 3,account_login: 0,change_password: 3,verifications_setting: 1)
                    @create_wallet = UserWallet.create(currency: 'USD',user_id: @user.id.to_i,amount: 0,status: 1,uuid: SecureRandom.hex(6))
                  end
                  cookies.permanent[:auth_token] = @user.auth_token
                  #cookies.permanent[:device_id] = @device_id
                  @user.loginattempts += 1
                  @user.failedattempts = 0
                  @user.save
                  session[:token_code] = @token
                  session[:last_visit] = Time.now
                   # get last signin to check if current ip is different from last ip send email to user
                  @last_ip = Watchdog.where("user_id =? ", @user.id ).last
                  if (@last_ip)
                     @current_ip = request.env['REMOTE_ADDR']
                     @db = MaxMindDB.new('./GeoLite2-Country.mmdb')
                     @last_ip_country = @db.lookup(@last_ip.ipaddress)                  
                     @current_ip_country = @db.lookup(@current_ip) 
                     if @last_ip_country.country.name != @current_ip_country.country.name
                        UserMailer.differentip(@user,@current_ip).deliver_later
                     end 
                  end
                  #@rem_token = Clearance::Token.new
                  #cookies.permanent[:remember_token] = @rem_token
                  #Login.create(user_id: @user.id, ip_address: request.remote_ip,user_agent: request.user_agent,device_id: @device_id, :operation_type => "sign_in", :remember_token => @rem_token)
                  Watchdog.create(:user_id => @user.id,:ipaddress => request.env['REMOTE_ADDR'],:logintime => Time.now,:lastvisit => Time.now, :operation_type => "sign_in"  )
                  if @user.active_otp == 1
                  # UserMailer.confirmsignin(@user,@token).deliver_later
                  EmailJob.perform_async(user_mail:current_user.email,text:@token,subject:'confirm sign in')
                  end
                  redirect_to two_factor_path
                  #redirect_to google_auth_path
                  #redirect_back_or url_after_create
                else
                  @currentuser = User.where("email =?",params[:session][:email].to_s).first
                  if @currentuser != nil 
                    @currentuser.failedattempts += 1
                     if @currentuser.failedattempts >= 3
                       @currentuser.disabled = 1
                       @currentuser.unlock_token = @token
                       UserMailer.unlockaccount(@currentuser,@token).deliver_later
                     end
                     @currentuser.save
                     Login.create(user_id: @currentuser.id, ip_address: request.remote_ip, user_agent: request.user_agent, device_id: SecureRandom.uuid, :operation_type => "failed_sign_in")
                     Watchdog.create(:user_id => @currentuser.id, :ipaddress => request.env['REMOTE_ADDR'], :logintime => Time.now, :lastvisit => Time.now, :operation_type => "failed_sign_in"  )
                  end
                #flash.now.notice = status.failure_message
                render template: "sessions/new", status: :unauthorized
                end
           end
       end
    end

    # when user signedout, he is logeed out from all sessions
  
    def destroy
      @user_sessions = Login.where(:remember_token => cookies[:remember_token], :user_id => current_user.id).first
      @user_sessions.destroy
      cookies.delete(:auth_token)
      cookies.delete(:device_id)
      session[:refered_by] = nil
      session[:last_visit] = nil
      flash.now[:notice] = "Successfully signed out."
      sign_out
      redirect_to url_after_destroy
    end

    def destroy_one_session
      @user_sessions = Login.where(:remember_token => params[:remember_token], :user_id => current_user.id).first
      @user_sessions.destroy
      redirect_to current_user
    end

    def destroy_all_sessions
      @user_sessions = Login.where(user_id: current_user.id).all
      if (@user_sessions  != nil)
          @user_sessions.all.each do |user_sessions|
            user_sessions.destroy
          end
      end
      cookies.delete(:auth_token)
      cookies.delete(:device_id)
      session[:refered_by] = nil
      session[:last_visit] = nil
      flash.now[:notice] = "Successfully signed out."
      sign_out
      redirect_to url_after_destroy
    end
  
    def new
      render template: "sessions/new"
    end

    def sign_in(user, &block)
      @current_user = user
      if current_user.present?
        status = "success"
      else
        status = "failed"
      end

      if current_user.present?
        @device_id = SecureRandom.uuid
        @rem_token = Clearance::Token.new
        cookies.permanent[:device_id] = @device_id
        Login.create(user_id: user.id, ip_address: request.remote_ip,user_agent: request.user_agent, :operation_type => "sign_in", :device_id => @device_id, :remember_token => @rem_token)
        cookies[:remember_token] = @rem_token
      else
        @current_user = nil
      end

      if block_given?
        block.call(status)
      end
    end


    def sign_out
      if signed_in?
        current_user.reset_remember_token!
      end

      current_user = nil
      cookies.delete (:remember_token)
    end
  
    #def user_from_remember_token(token)
     # @rem_token = Login.where(remember_token: token).first
      #Clearance.configuration.user_model.where(id: @rem_token.user_id).first
    #end

    #def remember_token
     # cookies[:remember_token]
    #end
  
    private
  
    def redirect_signed_in_users
      if signed_in?
        redirect_to url_for_signed_in_users
      end
    end
  
    def url_after_create
      Clearance.configuration.redirect_url
    end
  
    def url_after_destroy
      sign_in_url
    end
  
    def url_for_signed_in_users
      url_after_create
    end
  end