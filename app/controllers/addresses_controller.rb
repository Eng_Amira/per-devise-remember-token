class AddressesController < ApplicationController
  before_action :set_address, only: [:show, :edit, :update, :destroy]


  # GET a spacific address and display it
  # @param [Integer] id address unique ID.
  # @param [Integer] user_id the user unique id.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(true , false).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET a list of spacific user addresses
  # @param [Integer] user_id the user unique id.
  # @return [id] address unique ID.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(true , false).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def user_addresses
    @addresses = Address.where(:user_id => current_user.id.to_i).all
  end

  # GET a new address
  # @param [id] address unique ID (Created automatically).
  # @param [Integer] user_id the user unique id.
  # @param [String] address the detailed address.
  # @param [String] country the user address country.
  # @param [String] governorate the user address governorate.
  # @param [String] city the user address city.
  # @param [String] street the user address street.
  # @param [Boolean] default_address defualt address for a user(true , false).
  def new
    @address = Address.new
  end

  # GET an existing address and edit params
  # @param [String] address the detailed address.
  # @param [String] country the user address country.
  # @param [String] governorate the user address governorate.
  # @param [String] city the user address city.
  # @param [String] street the user address street.
  # @param [Boolean] default_address defualt address for a user(true , false).
  def edit
  end

  # POST a new address and save it
  # @return [id] address unique ID (Created automatically).
  # @return [user_id] the user unique id.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(true , false).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @address = Address.new(address_params)
    @address.user_id = current_user.id.to_i
    respond_to do |format|
      if @address.save
        format.html { redirect_to @address, notice: 'Address was successfully created.' }
        format.json { render :show, status: :created, location: @address }
      else
        format.html { render :new }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # Change an existing address params(address,country,governorate,governorate,street,default_address)
  # @return [id] address unique ID.
  # @return [user_id] the user unique id.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(true , false).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    respond_to do |format|
      if @address.update(address_params)
        format.html { redirect_to @address, notice: 'Address was successfully updated.' }
        format.json { render :show, status: :ok, location: @address }
      else
        format.html { render :edit }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE an existing address
  # @param [id] address unique ID.
  def destroy
    respond_to do |format|
        @addresses = Address.where(:user_id => @address.user_id).all
        if  @addresses.count == 1
          format.html { redirect_to addresses_url, notice: 'Cannot Delete this last Address' }
        else

          if @address.default_address == true
            format.html { redirect_to addresses_url, notice: 'Cannot Delete a Default Address' }
          else
            @address.destroy
            format.html { redirect_to addresses_url, notice: 'Address was successfully destroyed.' }
            format.json { head :no_content }
          end

        end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address
      @address = Address.where(user_id: current_user.id.to_i, id: params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def address_params
      params.require(:address).permit(:user_id, :address, :country, :governorate, :city, :street, :default_address)
    end
end
