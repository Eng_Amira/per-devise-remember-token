class WatchdogsController < ApplicationController
  before_action :require_login 
  before_action :set_watchdog, only: [:show, :edit, :update, :destroy]

  

  # show details of watchdog
  # works only if current user is an admin
  # @return [Integer] user_id
  # @return [DateTime] logintime
  # @return [String] ipaddress	
  # @return [DateTime] lastvisit
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def show
    if current_user.id != @watchdog.user_id.to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # show log of specific user
  # works only if current user is an admin
  # @return [String] username
  # @return [String] ipaddress	
  # @return [String] details
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def user_log
    if current_user.id == params[:id].to_i
      @log = Watchdog.where("user_id =? ",params[:id]).joins("INNER JOIN users ON users.id = watchdogs.user_id").distinct.all.order('created_at DESC')
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # POST /watchdogs
  # POST /watchdogs.json
  def create
    @watchdog = Watchdog.new(watchdog_params)

    respond_to do |format|
      if @watchdog.save
        format.html { redirect_to @watchdog, notice: 'Watchdog was successfully created.' }
        format.json { render :show, status: :created, location: @watchdog }
      else
        format.html { render :new }
        format.json { render json: @watchdog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /watchdogs/1
  # PATCH/PUT /watchdogs/1.json
  def update
      respond_to do |format|
        if @watchdog.update(watchdog_params)
          format.html { redirect_to @watchdog, notice: 'Watchdog was successfully updated.' }
          format.json { render :show, status: :ok, location: @watchdog }
        else
          format.html { render :edit }
          format.json { render json: @watchdog.errors, status: :unprocessable_entity }
        end
      end
  end

  # DELETE /watchdogs/1
  # DELETE /watchdogs/1.json
  def destroy
       @watchdog.destroy
       respond_to do |format|
         format.html { redirect_to watchdogs_url, notice: 'Watchdog was successfully destroyed.' }
         format.json { head :no_content }
       end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_watchdog
      @watchdog = Watchdog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def watchdog_params
      params.require(:watchdog).permit(:user_id, :logintime, :ipaddress, :lastvisit)
    end
end
