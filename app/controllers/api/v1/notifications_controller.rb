module Api
  module V1
    class NotificationsController < ApplicationController
      before_action :set_notification, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token

      # GET /notifications
      def index
        @notifications = Notification.all
        respond_to do |format|
          format.json { render json: @notifications }
        end
        # =begin
        # @api {get} /api/v1/notifications 1-Request notifications List
        # @apiVersion 0.3.0
        # @apiName GetNotifications
        # @apiGroup Notifications
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/notifications
        # @apiSuccess {Number} id notification unique ID.
        # @apiSuccess {Number} user_id notification user unique ID.
        # @apiSuccess {String} title notification title.
        # @apiSuccess {String} description notification content.
        # @apiSuccess {Number} notification_type notification service type (1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "user_id": 1,
        #       "title": "Balance transfer",
        #       "description": "10 USD has been successfully transferred to user 2",
        #       "notification_type": 3,
        #       "created_at": "2018-09-16T11:46:40.994Z",
        #       "updated_at": "2018-09-16T11:46:40.994Z"
        #   },
        #   {
        #       "id": 2,
        #       "user_id": 2,
        #       "title": "Balance transfer",
        #       "description": "5 USD has been successfully transferred to user 1",
        #       "notification_type": 1,
        #       "created_at": "2018-09-16T11:48:44.618Z",
        #       "updated_at": "2018-09-16T11:48:44.618Z"
        #   }
        # ]
        # @apiError MissingToken invalid Token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end

      # GET /notifications/1
      def show

        flash[:error]= "Notification Not Found"
        respond_to do |format|
          if @notification != nil
            format.json { render json: @notification }
          else
            format.json { render json: flash }
          end
        end

        # =begin
        # @api {get} /api/v1/notifications/{:id} 2-Request a specific notification
        # @apiVersion 0.3.0
        # @apiName GetNotification
        # @apiGroup Notifications
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/notifications/2
        # @apiParam {Number} id notification unique ID.
        # @apiSuccess {Number} user_id notification user unique ID.
        # @apiSuccess {String} title notification title.
        # @apiSuccess {String} description notification content.
        # @apiSuccess {Number} notification_type notification service type(1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 2,
        #       "user_id": 2,
        #       "title": "Balance transfer",
        #       "description": "5 USD has been successfully transferred to user 1",
        #       "notification_type": 1,
        #       "created_at": "2018-09-16T11:48:44.618Z",
        #       "updated_at": "2018-09-16T11:48:44.618Z"
        #   }
        # ]
        # @apiError NotificationNotFound The id of this notification was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "Notification Not Found"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end
      end

      # GET list of notifications log of a specific user
      def user_notifications
        @user_notifications = Notification.where(user_id: 1).all
        flash[:error]= "User Not Found"
        respond_to do |format|
          if @user_notifications != []
            format.json { render json: @user_notifications }
          else
            format.json { render json: flash }
          end
        end

        # =begin
        # @api {get} /api/v1/user_notifications 3-Request a notifications list of a specific user
        # @apiVersion 0.3.0
        # @apiName GetUserNotification
        # @apiGroup Notifications
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/user_notifications
        # @apiParam {Number} user_id notification user unique ID.
        # @apiSuccess {Number} id notification unique ID.
        # @apiSuccess {Number} user_id notification user unique ID.
        # @apiSuccess {String} title notification title.
        # @apiSuccess {String} description notification content.
        # @apiSuccess {Number} notification_type notification service type(1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 4,
        #       "user_id": 1,
        #       "title": "Balance transfer",
        #       "description": "5 USD has been successfully transferred to user 2",
        #       "notification_type": 3,
        #       "created_at": "2018-09-16T12:38:39.493Z",
        #       "updated_at": "2018-09-16T12:38:39.493Z"
        #   },
        #   {
        #       "id": 5,
        #       "user_id": 1,
        #       "title": "Balance transfer",
        #       "description": "10 USD has been successfully transferred to user 2",
        #       "notification_type": 3,
        #       "created_at": "2018-09-16T13:21:44.439Z",
        #       "updated_at": "2018-09-16T13:21:44.439Z"
        #   }
        # ]
        # @apiError UserNotFound The id of this User was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "User Not Found"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end

      def search

        @search = params[:search]
        @searchdatefrom = params[:searchdatefrom]
        @searchdateto = params[:searchdateto]
        @notification_type = params[:notification_type]
        @search_user = params[:search_user]
    
        @notifications = Notification.all
        @notifications = @notifications.where("notifications.created_at >= ? ", @searchdatefrom)   if  @searchdatefrom.present?
        @notifications = @notifications.where("notifications.created_at <= ? ", @searchdateto)   if  @searchdateto.present?
        @notifications = @notifications.where("notifications.created_at <= ? AND notifications.created_at >= ? ", @searchdateto,@searchdatefrom)   if  (@searchdateto.present? and  @searchdatefrom.present?)
        @notifications = @notifications.where(["title LIKE  ? OR description LIKE  ?","%#{@search}%","%#{@search}%"]).distinct.all if  @search.present?
        @notifications = @notifications.where("notifications.notification_type = ? ", @notification_type)   if  @notification_type.present?
        @notifications = @notifications.where("notifications.user_id = ? ", @search_user)   if  @search_user.present?
        
        respond_to do |format|
          if @notifications != []
            format.json { render json: @notifications }
          else
            flash[:error] = "There are no results for your search."
            format.json { render json: flash }
          end
        end

        # =begin 
        # @api {get} /api/v1/search_notifications?&search={:search}&search_user={:user_id}&notification_type={:notification_type}&searchdatefrom={:created_at_from}&searchdateto={:created_at_to} 4-Search for an existing notification
        # @apiVersion 0.3.0
        # @apiName GetNotificationSearch
        # @apiGroup Notifications
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/search_notifications?=&search=balance&search_user=1&notification_type=1&searchdatefrom=2018-09-16&searchdateto=2018-09-17
        # @apiParam {String} search search word that you want to find(title , description).
        # @apiParam {Date} searchdatefrom search created at date from.
        # @apiParam {Date} searchdateto search created at date to.
        # @apiParam {Integer} notification_type notification service type(1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Integer} search_user search user ID than you want.
        # @apiSuccess {Number} id notification unique ID.
        # @apiSuccess {Number} user_id notification user unique ID.
        # @apiSuccess {String} title notification title.
        # @apiSuccess {String} description notification content.
        # @apiSuccess {Number} notification_type notification service type(1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "user_id": 1,
        #       "title": "Balance transfer",
        #       "description": "10 USD has been successfully transferred to user 2",
        #       "notification_type": 3,
        #       "created_at": "2018-09-16T11:46:40.994Z",
        #       "updated_at": "2018-09-16T11:46:40.994Z"
        #   },
        #   {
        #       "id": 2,
        #       "user_id": 2,
        #       "title": "Balance transfer",
        #       "description": "5 USD has been successfully transferred to user 1",
        #       "notification_type": 1,
        #       "created_at": "2018-09-16T11:48:44.618Z",
        #       "updated_at": "2018-09-16T11:48:44.618Z"
        #   }
        # ]
        # @apiError NoResults There are no results for your search. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 No Results
        #   {
        #     "error": "There are no results for your search."
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end

      # DELETE /notifications/1
      def destroy
        respond_to do |format|
          if @notification.destroy
            flash[:success]= "Notification was successfully destroyed" 
            format.json { render json: flash }
          else
            flash[:error]= "Notification Not Found" 
            format.json { render json: flash }
          end
        end

        # =begin
        # @api {Delete} /api/v1/notifications{:id} 5-Delete an existing notification
        # @apiVersion 0.3.0
        # @apiName DeleteNotification
        # @apiGroup Notifications
        # @apiExample Example usage:
        # curl -X DELETE \
        # http://localhost:3000/api/v1/notifications/4 \
        # @apiParam {Number} id the notification unique id.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #   {
        #     "success": "Notification was successfully destroyed"
        #   }
        # @apiError NotificationNotFound The id of the notification was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "Notification Not Found"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_notification
          @notification = Notification.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def notification_params
          params.require(:notification).permit(:user_id, :title, :description, :notification_type)
        end
    end
  end
end
