module Api
  module V1
    class NotificationsSettingsController < ApplicationController
      before_action :set_notifications_setting, only: [:edit, :update]
      skip_before_action :verify_authenticity_token

      # GET /notifications_settings
      def index

        @notifications_settings = NotificationsSetting.all
        respond_to do |format|
          format.json { render json: @notifications_settings }
        end

        # =begin
        # @api {get} /api/v1/notifications_settings 1-Request notifications settings List
        # @apiVersion 0.3.0
        # @apiName GetNotificationsSettings
        # @apiGroup Notifications Settings
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/notifications_settings
        # @apiSuccess {Number} id notification setting unique ID.
        # @apiSuccess {Number} user_id notification setting user ID.
        # @apiSuccess {Number} money_transactions Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} pending_transactions pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} transactions_updates current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} help_tickets_updates help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} tickets_replies new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} account_login login to user account(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} change_password change password(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} verifications_setting when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "user_id": 1,
        #       "money_transactions": 3,
        #       "pending_transactions": 1,
        #       "transactions_updates": 1,
        #       "help_tickets_updates": 3,
        #       "tickets_replies": 1,
        #       "account_login": 0,
        #       "change_password": 1,
        #       "verifications_setting": 3,
        #       "created_at": "2018-08-09T11:20:01.741Z",
        #       "updated_at": "2018-08-09T11:20:01.741Z"
        #   },
        #   {
        #       "id": 2,
        #       "user_id": 2,
        #       "money_transactions": 1,
        #       "pending_transactions": 2,
        #       "transactions_updates": 1,
        #       "help_tickets_updates": 1,
        #       "tickets_replies": 3,
        #       "account_login": 0,
        #       "change_password": 0,
        #       "verifications_setting": 2,
        #       "created_at": "2018-08-09T11:21:11.770Z",
        #       "updated_at": "2018-08-09T11:22:21.779Z"
        #   }
        # ]
        # @apiError MissingToken invalid Token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end


      # GET /notifications_settings/new
      def new
        @notifications_setting = NotificationsSetting.new
      end

      # GET /notifications_settings/id/edit
      def edit
      end

      # POST /notifications_settings
      def create

        # =begin
        # @api {post} /api/v1/notifications_settings 2-Create a Notification Settings
        # @apiVersion 0.3.0
        # @apiName PostNotificationsSettings
        # @apiGroup Notifications Settings
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/v1/notifications_settings \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        # "user_id": 4,
        # "money_transactions": 1,
        # "pending_transactions": 1,
        # "transactions_updates": 3,
        # "help_tickets_updates": 0,
        # "tickets_replies": 2,
        # "account_login": 0,
        # "change_password": 1,
        # "verifications_setting": 2
        # }'
        # @apiParam {Number} user_id notification setting user ID.
        # @apiParam {Number} money_transactions Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} pending_transactions pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} transactions_updates current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} help_tickets_updates help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} tickets_replies new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} account_login login to user account(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} change_password change password(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} verifications_setting when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} id notification setting unique ID.
        # @apiSuccess {Number} user_id notification setting user ID.
        # @apiSuccess {Number} money_transactions Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} pending_transactions pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} transactions_updates current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} help_tickets_updates help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} tickets_replies new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} account_login login to user account(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} change_password change password(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} verifications_setting when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # {
        #   "id": 4,
        #   "user_id": 4,
        #   "money_transactions": 1,
        #   "pending_transactions": 1,
        #   "transactions_updates": 3,
        #   "help_tickets_updates": 0,
        #   "tickets_replies": 2,
        #   "account_login": 0,
        #   "change_password": 1,
        #   "verifications_setting": 2,
        #   "created_at": "2018-08-11T07:51:33.838Z",
        #   "updated_at": "2018-08-11T07:51:33.838Z"
        # }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError ExistingUser User id has already been taken.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 Existing User ID
        #   {
        #     "error": "has already been taken"
        #   }
        # =end

        @notifications_setting = NotificationsSetting.new(notifications_setting_params)

        respond_to do |format|
          if @notifications_setting.save
            format.json { render json: @notifications_setting, status: :created, location: @notifications_setting }
          else
            format.json { render json: @notifications_setting.errors, status: :unprocessable_entity }
          end
        end
        
      end

      # PATCH/PUT /notifications_settings/id
      def update

        # =begin
        # @api {put} /api/v1/notifications_settings/{:id} 3-Update an existing Notification Settings
        # @apiVersion 0.3.0
        # @apiName PutNotificationSettings
        # @apiGroup Notifications Settings
        # @apiExample Example usage:
        # curl -X PUT \
        # http://localhost:3000/api/v1/notifications_settings/3 \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        # "money_transactions": 2,
        # "pending_transactions": 1,
        # "transactions_updates": 3,
        # "help_tickets_updates": 3,
        # "tickets_replies": 2,
        # "account_login": 1,
        # "change_password": 0,
        # "verifications_setting": 3
        # }'
        # @apiParam {Number} money_transactions Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} pending_transactions pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} transactions_updates current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} help_tickets_updates help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} tickets_replies new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} account_login login to user account(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} change_password change password(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiParam {Number} verifications_setting when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} id notification setting unique ID.
        # @apiSuccess {Number} user_id notification setting user ID.
        # @apiSuccess {Number} money_transactions Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} pending_transactions pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} transactions_updates current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} help_tickets_updates help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} tickets_replies new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} account_login login to user account(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} change_password change password(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} verifications_setting when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 3,
        #   "money_transactions": 2,
        #   "pending_transactions": 1,
        #   "transactions_updates": 3,
        #   "help_tickets_updates": 3,
        #   "tickets_replies": 2,
        #   "account_login": 1,
        #   "change_password": 0,
        #   "verifications_setting": 3,
        #   "user_id": 3,
        #   "created_at": "2018-08-11T07:49:20.457Z",
        #   "updated_at": "2018-08-11T08:20:57.508Z"
        # }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError NotificationSettingsNotFound The id of the Notification Settings was not found. 
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "NotificationSettingsNotFound"
        #   }
        # =end

        respond_to do |format|
          if @notifications_setting.update(notifications_setting_params)
            format.json { render json: @notifications_setting, status: :ok, location: @notifications_setting }
          else
            format.json { render json: @notifications_setting.errors, status: :unprocessable_entity }
          end
        end

      end

      def add_defualt_notifications

        # =begin
        # @api {get} /api/v1/add_defualt_notifications?id={:id} 4-Return to a defualt notification setting values
        # @apiVersion 0.3.0
        # @apiName ReturnDefualtNotification
        # @apiGroup Notifications Settings
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/add_defualt_notifications?id=4
        # @apiParam {Number} id notification setting unique ID.
        # @apiSuccess {Number} id notification setting unique ID.
        # @apiSuccess {Number} user_id notification setting user ID.
        # @apiSuccess {Number} money_transactions Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} pending_transactions pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} transactions_updates current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} help_tickets_updates help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} tickets_replies new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} account_login login to user account(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} change_password change password(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Number} verifications_setting when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail & message).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "money_transactions": 1,
        #   "pending_transactions": 2,
        #   "transactions_updates": 2,
        #   "help_tickets_updates": 2,
        #   "tickets_replies": 3,
        #   "account_login": 3,
        #   "change_password": 0,
        #   "verifications_setting": 0,
        #   "id": 4,
        #   "user_id": 4,
        #   "created_at": "2018-08-11T07:51:33.838Z",
        #   "updated_at": "2018-08-12T08:26:34.043Z"
        # }
        # @apiError NotificationSettingNotFound The ID of this Notification Setting was not found. 
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "This Notification Setting Not Found"
        #   }
        # =end

        @notification = NotificationsSetting.find(params[:id])
        @defualt = NotificationsSetting.where(:user_id => 0).first
        @notification.money_transactions = @defualt.money_transactions
        @notification.pending_transactions = @defualt.pending_transactions
        @notification.transactions_updates = @defualt.transactions_updates
        @notification.help_tickets_updates = @defualt.help_tickets_updates
        @notification.tickets_replies = @defualt.tickets_replies
        @notification.account_login = @defualt.account_login
        @notification.change_password = @defualt.change_password
        @notification.verifications_setting = @defualt.verifications_setting
        flash[:error]= "This Notification Setting Not Found"
       
        respond_to do |format|
          if  @notification.save
            format.json { render json: @notification }
          else
            format.json { render json: flash }
          end
        end

      end


      private
        # Use callbacks to share common setup or constraints between actions.
        def set_notifications_setting
          @notifications_setting = NotificationsSetting.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def notifications_setting_params
          params.require(:notifications_setting).permit(:user_id, :money_transactions, :pending_transactions, :transactions_updates, :help_tickets_updates, :tickets_replies, :account_login, :change_password, :verifications_setting)
        end
    end

  end

end
