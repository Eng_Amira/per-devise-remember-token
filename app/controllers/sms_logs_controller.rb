class SmsLogsController < ApplicationController

    # GET list of SMS Logs
    # @return [user_id] The receiver user id.
    # @return [pinid] SMS return pinId that was coming from the SMS provider.
    # @return [sms_type] SMS type (send_pin,verify_code,text_sms).
    # @return [status] SMS status(true = Verified , false = Not Verified).
    # @return [created_at] Date Sent.
    # @return [updated_at] Date Updated.
    def index
      @sms_logs = SmsLog.all
    end

end