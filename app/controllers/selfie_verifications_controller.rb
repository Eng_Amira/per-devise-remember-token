class SelfieVerificationsController < ApplicationController
  before_action :set_selfie_verification, only: [:show, :edit, :update, :destroy]
  before_action :require_login


  # show details of Selfie Verification of specific user
  # @param [Integer] id 
  # @return [String] username
  # @return [Blob] avatar
  # @return [Integer] status
  # @return [String] note
  # @return [datetime] created_at
  # @return [datetime] updated_at 
  def show
    if current_user.id != @selfie_verification.user_id.to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new Selfie Verification
  # each user can create only one Selfie verification
  def new
    @current_user_verification = SelfieVerification.where("user_id =?", current_user.id.to_i).first
    if @current_user_verification != nil
      redirect_to root_path , notice: "not allowed"
    else
    @selfie_verification = SelfieVerification.new
    end
  end

  # edit Selfie Verification
  # user can edit his Selfie verification only if status is not verified
  # @param [Integer] id
  def edit
    if (@selfie_verification.status == "Verified") or (current_user.id != @selfie_verification.user_id.to_i)
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new Selfie Verification
  # @param [Blob] avatar
  # @return [Integer] id
  # @return [String] username
  # @return [Blob] avatar
  # @return [Integer] status
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    @selfie_verification = SelfieVerification.new(selfie_verification_params)
    @selfie_verification.user_id = current_user.id
    respond_to do |format|
      if @selfie_verification.save
        format.html { redirect_to @selfie_verification, notice: 'Selfie verification was successfully created.' }
        format.json { render :show, status: :created, location: @selfie_verification }
      else
        format.html { render :new }
        format.json { render json: @selfie_verification.errors, status: :unprocessable_entity }
      end
    end
  end

  # edit Selfie Verification,
  # only admins can edit status and note.
  # @param [Blob] avatar
  # @param [Integer] status
  # @param [String] note
  # @return [Integer] id
  # @return [String] username
  # @return [Blob] avatar
  # @return [Integer] status
  # @return [String] note
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def update
    respond_to do |format|
      if @selfie_verification.update(selfie_verification_params)
        format.html { redirect_to @selfie_verification, notice: 'Selfie verification was successfully updated.' }
        format.json { render :show, status: :ok, location: @selfie_verification }
      else
        format.html { render :edit }
        format.json { render json: @selfie_verification.errors, status: :unprocessable_entity }
      end
    end
  end

  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_selfie_verification
      @selfie_verification = SelfieVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def selfie_verification_params
      params.require(:selfie_verification).permit(:user_id, :note, :avatar, :status)
    end
end
