class UserWalletsController < ApplicationController
  before_action :set_user_wallet, only: [:show]


  # GET a spacific User Wallet and display it
  # @param [Integer] id User Wallet unique ID.
  # @return [currency] Users Wallets Currency (default = USD).
  # @return [amount] User Wallet amount.
  # @return [user_id] Wallet User ID.
  # @return [status] User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [uuid] User Wallet Secure random number (Created automatically and must be 12 digits and letters).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def user_wallet_balance
    @user_wallet = UserWallet.where(:user_id => current_user.id.to_i).first
  end


  # GET a new pin code SMS
  # @param [Integer] tel User phone number preceded by country code.
  def send_sms
  end

  # POST a new pin code SMS and send it to user
  # @return [pinId] SMS return pinId that was coming from the SMS provider.
  def send_sms_post
    SMSJob.perform_async(tel:params[:tel],user_id: current_user.id)
    redirect_to(verifysms_path)
  end

  # GET verify SMS code
  # @param [Integer] code SMS code that was sent to the user.
  def verifysms
  end

  # POST verify SMS code
  # @return [verified] Code Verification status(ture , false).
  def verifysms_post

    @code = params[:code].to_i
    @applications_list = SMSService.new(code:@code).call
    @userinfo = UserInfo.where("user_id =?", current_user.id ).first

    if @applications_list["verified"] ==  true
      @userinfo = UserInfo.where("user_id =?", current_user.id ).first
      if @userinfo != nil 
        @userinfo.update(:mobile => 1 )
        if @userinfo.address_verification_id == 1 and @userinfo.nationalid_verification_id == 1 and @userinfo.selfie_verification_id == 1
          @userinfo.update(:status => "Verified")
        end
      else
        UserInfo.create(:user_id => current_user.id , :address_verification_id => 0, :nationalid_verification_id => 0, :selfie_verification_id => 0, :status => "UnVerified")
      end
      redirect_to root_path, notice: 'Valid code, successfully activated'  
    else 
      redirect_to root_path, notice: 'Wrong code Try again later'
    end

  end

  # Transfer Balances between users wallets
  # @param [Integer] userfrom The sender user ID.
  # @param [Integer] userto The receiver user ID.
  # @param [Float] transferamount The transferred amount between two users.
  def transfer_balance   
  end

  # Post a transferred balance between two users
  # @return [userfrom] The sender user ID.
  # @return [userto] The receiver user ID.
  # @return [transferamount] The transferred amount between two users.
  # @return [new_balance_from] The sender new balance after transfer.
  # @return [new_balance_to] The receiver new balance after transfer.
  def transfer_balancepost
    @user_from = current_user.id.to_i
    @user = User.where("username = ? OR account_number = ?", params[:userto],params[:userto]).first
    @user_to = @user.id
    @transfer_amount = params[:transferamount]
    @message = UserWallet.checktransfer(@user_from.to_i,@user_to.to_i,@transfer_amount)
    if @message == ""
      ActiveRecord::Base.transaction do
      @user_wallet_from = UserWallet.where(user_id: @user_from.to_i).first
      @user_wallet_to = UserWallet.where(user_id: @user_to.to_i).first
      @new_balance_from = @user_wallet_from.amount.to_f - @transfer_amount.to_f
      @new_balance_to = @user_wallet_to.amount.to_f + @transfer_amount.to_f
      @user_wallet_from.update(:amount => @new_balance_from )
      @user_wallet_to.update(:amount => @new_balance_to )
      @smstext = "#{@transfer_amount} USD has been successfully transferred to user #{@user.username}"
      @tel = current_user.telephone
      @user_mail = current_user.email
      @user_notification_setting = NotificationsSetting.where(user_id: @user_from).first
      if @user_notification_setting.money_transactions != 0
        @aa = Notification.create(user_id: @user_from ,title: "Balance transfer", description: @smstext , notification_type: @user_notification_setting.money_transactions)
      end
      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@tel,@smstext)
        SmsLog.create(:user_id => @user_from, :pinid => @smstext,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@tel,@smstext)
        SmsLog.create(:user_id => @user_from, :pinid => @smstext,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext)
      end
      
      redirect_to(user_wallet_balance_path,:notice => "Balance was successfully transferred")
      end
    else
      redirect_to(transfer_balance_path,:notice => @message )
    end
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_wallet
      @user_wallet = UserWallet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_wallet_params
      params.require(:user_wallet).permit(:currency, :amount, :user_id, :status, :uuid)
    end
end
