class CardsController < ApplicationController

  # Get list of all the user cards
  # @return [id] card unique ID (Created automatically).
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [user_id] card user ID (defualt = 1).
  # @return [invoice_id] card invoice ID (defualt = 0).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def user_cards
    @user_cards = Card.where(user_id: current_user.id.to_i).all
  end

  # Create a new card with a spacific Category value
  # @param [Integer]  value card value. (Created automatically from Card Category value).
  # @return [id] card unique ID (Created automatically).
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [user_id] card user ID (defualt = 1).
  # @return [invoice_id] card invoice ID (defualt = 0).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def buy_card

    @new_card =  Card.create(:value => params[:val],:expired_at => Date.today + 3.days,:status => 1,:user_id => current_user.id.to_i)
    @log = CardsLog.create(:user_id => current_user.id.to_i,:action_type => 1,:ip => request.remote_ip,:card_id => @new_card.id)

    if @new_card
      redirect_to(display_cards_categories_path,:notice => 'Card was successfully created')
    end
    
  end

  def remittance
  end

  def payment_method
    @local_banks = CompanyBank.where(:bank_category => "local_bank", :status => 1).all
    @electronic_banks = CompanyBank.where(:bank_category => "electronic_bank", :status => 1).all
    @digital_banks = CompanyBank.where(:bank_category => "digital_bank", :status => 1).all
  end
  
  def payment_data

    @selectedbank = params[:selectedbank]
    @value = params[:val].to_f
    @bank = CompanyBank.where(:bank_key => @selectedbank ).first
    @result = @value * (@bank.ratio / 100) + @bank.fees
    render :json => { 'value': @result ,'bankdata': @bank }
 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_card
      @card = Card.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def card_params
      params.require(:card).permit(:number, :value, :status, :expired_at, :user_id, :invoice_id)
    end
end
