json.extract! country, :id, :short_code, :Full_Name, :Phone_code, :Currency, :language, :active, :created_at, :updated_at
json.url country_url(country, format: :json)
