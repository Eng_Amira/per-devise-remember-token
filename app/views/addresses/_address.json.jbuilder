json.extract! address, :id, :user_id, :address, :country, :governorate, :city, :street, :default_address, :created_at, :updated_at
json.url address_url(address, format: :json)
