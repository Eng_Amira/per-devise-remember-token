json.extract! card, :id, :number, :value, :status, :expired_at, :user_id, :invoice_id, :created_at, :updated_at
json.url card_url(card, format: :json)
