# app/jobs/sms_job.rb

class SMSJob

    include SuckerPunch::Job

    # SMS Provider Background Job
    # @param [Integer] tel The receiver user phone number.
    # @param [String] smstext Text SMS content.
    # @param [Integer] code SMS Verification Pin Code.
    def perform(tel:0,smstext:"",code:0,user_id:1)     
        @tel = tel
        @smstext = smstext
        @code = code
        @user_id = user_id
        SMSService.new(tel:@tel,smstext:@smstext,code:@code,user_id:@user_id).call
    end

end
