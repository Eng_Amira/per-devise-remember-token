require 'rake'

# task :cron => :environment do
 
#     Cart.cornjob

# end





namespace :cron do
  desc "TODO"

  task test:  :environment do
    @unconfirmed_users = User.where("status =? ",0).all
    @timeago = (Time.now - 15.to_i.minutes)
    @unconfirmed_users.all.each do |user|       
      @user_created = user.created_at
        if @user_created < @timeago
           @user_log = Watchdog.where("user_id = ?",user.id).all
           if @user_log
              @user_log.all.each do |log|
               log.destroy
              end
           end
           @invitation_record = AffilateProgram.where("user_id =?" ,user.id).first
           if @invitation_record
                @invitation_record.destroy
           end
           @user_verification = Verification.where("user_id =?" ,user.id).first
           if @user_verification
              @user_verification.destroy
           end
           user.destroy 
        end
    end
  end 

end

