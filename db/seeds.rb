# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Country.create([{
    short_code: "EGY",
    Full_Name: "Egypt",
    Phone_code: "002",
    Currency:  "EGP",
    language: "Arabic",
    active: "1",
            },
{
    short_code: "SAU",
    Full_Name: "Saudi Arabia	",
    Phone_code: "00966",
    Currency:  "SAR",
    language: "Arabic",
    active: "1",
            }
        ]) 

Admin.create([{
            email: "amira.elfayome@servicehigh.com",
            password: "Amira123456",
            username: "Amira",
            status: 1,
            account_number: "PS100000001",
            remember_token: Clearance::Token.new,
            country_id: 1
        }
])        
        
tansfer_balance_ratio = WalletsTransferRatio.create([{ key: 'max_transfer', value: 10, description: 'maximum wallets transfer balance' }, { key: 'min_transfer', value: 2, description: 'minimum wallets transfer balance' }])

defualt_notification = NotificationsSetting.create(user_id: 0,money_transactions: 1,pending_transactions: 2,transactions_updates: 2,help_tickets_updates: 3,tickets_replies: 3,account_login: 0,change_password: 3,verifications_setting: 1)

