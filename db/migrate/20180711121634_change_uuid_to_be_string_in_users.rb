class ChangeUuidToBeStringInUsers < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :uuid, :string
  end
end
