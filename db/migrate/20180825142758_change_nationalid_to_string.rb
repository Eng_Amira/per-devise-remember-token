class ChangeNationalidToString < ActiveRecord::Migration[5.2]
  def change
      change_column :nationalid_verifications, :national_id, :string
  end
end
