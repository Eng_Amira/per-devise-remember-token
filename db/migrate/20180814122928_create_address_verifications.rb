class CreateAddressVerifications < ActiveRecord::Migration[5.2]
  def change
    create_table :address_verifications do |t|
      t.integer :user_id
      t.integer :country_id
      t.string :city
      t.string :state
      t.string :street
      t.string :building
      t.string :number
      t.integer :status
      t.string :note

      t.timestamps
    end
  end
end
