class CreateSmsLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :sms_logs do |t|
      t.integer :user_id
      t.string :pinid
      t.boolean :status , :default => 0
      t.integer :sms_type

      t.timestamps
    end
  end
end
