class AddStatusToCompanyBanks < ActiveRecord::Migration[5.2]
  def change
    add_column :company_banks, :status, :integer
  end
end
