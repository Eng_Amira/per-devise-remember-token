class CreateCardsCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :cards_categories do |t|
      t.integer :card_value
      t.integer :active
      t.integer :order

      t.timestamps
    end
  end
end
