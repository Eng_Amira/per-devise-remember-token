class CreateCompanyBanks < ActiveRecord::Migration[5.2]
  def change
    create_table :company_banks do |t|
      t.string :bank_key
      t.string :bank_name
      t.string :encryptkey
      t.string :currency
      t.string :country
      t.string :city
      t.string :branch
      t.string :branch_code
      t.string :phone
      t.string :account_name
      t.string :account_email
      t.string :account_number
      t.string :swiftcode
      t.string :ibancode
      t.integer :visa_cvv
      t.string :bank_category
      t.string :bank_subcategory
      t.float :fees
      t.float :ratio
      t.string :logo
      t.date :expire_at

      t.timestamps
    end
  end
end
