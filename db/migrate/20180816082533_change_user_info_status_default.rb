class ChangeUserInfoStatusDefault < ActiveRecord::Migration[5.2]
  def change
    change_column :user_infos, :status, :integer, null: false, :default => 0
  end
end
